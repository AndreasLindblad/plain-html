

// workaround for 2 drawings - face drawing
let s = function(p) {
    let x = 100;
    let y = 100;
    p.setup = function() {
      p.createCanvas(600, 500);
    };
    
    p.draw = function() {
      p.background("pink");
    
    // face drawing
      p.fill(197, 140, 133);
      p.ellipse(300,300,220,260);
      p.fill(0,0,0,0);
      p.circle(260,240,20);
      p.circle(340,240,20);
      p.rect(240,220,40,40);
      p.rect(320,220,40,40);
      p.fill(255,0,0,150);
      p.ellipse(300,340,150,100);
      p.fill("white");
      p.rect(280,290,20,40);
      p.rect(300,290,20,40);
      p.fill("black");
      p.rect(210,170,180,30);
      p.rect(250,50,100,120);
      }  
    }
  var myp5 = new p5(s, "c1");
  
  //2nd sketch start
  let t = function(p){
    var myData;
    p.preload = function() {
    myData = p.loadJSON('data.json'); 
      }
    
    p.setup = function() {
      p.frameRate(1);
      p.createCanvas(800, 800);
    };
    
    // Data visualisation
    p.draw = function() {
      
      let r = 0;
      for (let r = 0; r < myData.topics.length; r++){
        delay(r)
        let textX = p.random(0,p.width*0.9);// 0.9 to keep text more centered
        let textY = p.random(0, p.height*0.9);
        p.textFont("Georgia")
        p.text(myData.topics[r], textX, textY);
      }//end for loop
      
      function delay(r) {
        setTimeout(() => {
        }, 1000);
      }   
    }//end draw 2 
  }//2nd sketch end
  var myp5 = new p5(t, "c2");
    
    